using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#pragma warning disable 649

namespace Zenject.SpaceFighter
{
    public class ControlsDisplay : MonoBehaviour
    {
        [SerializeField]
        float _leftPadding;

        [SerializeField]
        float _topPadding;

        [SerializeField]
        float _width;

        [SerializeField]
        float _height;

        public GameObject ui;
       public Player _player;
       public List<EnemyStateManager> _stateManager = new List<EnemyStateManager>();
        public void OnGUI()
        {

            var bounds = new Rect(_leftPadding, _topPadding, _width, _height);
            GUI.Label(bounds, "CONTROLS:  WASD to move, Mouse to aim, Left Mouse to fire");
        }

        public void Continue()
        {
            for (int i = 0; i < _stateManager.Count; i++)
            {
                _stateManager[i].ChangeState(EnemyStates.Attack);
            }
            _player._health = 100f;
            _player.Renderer.enabled = true;
            _player.IsDead = false;
            ui.SetActive(false);
        }

        public void Retry()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            ui.SetActive(false);
        }

       

        void OnEnemyKilled()
        {
        }
    }
}

